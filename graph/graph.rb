class Graph
  attr_reader :vertices
  def initialize()
    @vertices = []
  end
  
  def find_vertex(name)
    @vertices.find { |v| v.name == name }
  end

  def add_vertex(v)
    @vertices << v if (ve = self.find_vertex v.name).nil?
    ve || v
  end

  def transpose
    gt = Graph.new
    @vertices.each do |vertex|
      v2 = Vertex.new vertex.name
      vf = gt.add_vertex v2
      vertex.edges.each do |edge|
        v1 = Vertex.new edge.name
        vo = gt.add_vertex v1

        vo.add_edge vf
      end
    end
    gt
  end

  def show_matrix
    @vertices.each do |vertex|
      print "| #{vertex.name} ->"
      vertex.edges.each {|i| print " #{i.name} "}
      puts 
    end
  end

  def bp(vertex, numb_bp, num_invbp)
    vertex.visit
    numb_bp << vertex
    vertex.edges.each { |edge| self.bp edge, numb_bp, num_invbp unless edge.visited }
    num_invbp << vertex
  end

  def kosaraju
    numb_bp = []
    num_invbp = []
    @vertices.each { |vertex| self.bp vertex, numb_bp, num_invbp unless vertex.visited }

    gt = self.transpose

    puts 'CFC del Grafo'
    num_invbp.reverse_each do |v|
      gt.bp v, numb_bp, num_invbp
      unless (vertex = gt.find_vertex v.name).visited
        numb_bp = []
        gt.bp vertex, numb_bp, num_invbp
        numb_bp.each {|i| print " #{ i.name} " }
        puts 
      end
    end
  end

end
