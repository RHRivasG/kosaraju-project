class Vertex
  attr_accessor :name
  attr_reader :visited
  attr_reader :edges
  def initialize(name)
    @name = name
    @visited = false
    @edges = []
    @numb_bp = nil
    @num_invbp = nil
  end

  def visit
    @visited = true
  end

  def find_edge(name)
    @edges.find { |v| v.name == name }
  end

  def add_edge(edge)
    @edges << edge if (self.find_edge edge.name).nil?
  end

  def add_numb_bp(num)
    @numb_bp = num
  end

  def add_num_inv_bp(num)
    @num_invbp = num
  end
end
