require_relative "../graph/graph.rb"
require_relative "../graph/vertex.rb"

class GraphFactory
  def select_file (name)
    return '/../examples/example-1.txt' if name == 1;
    return '/../examples/example-2.txt' if name == 2;
    return '/../examples/example-3.txt' if name == 3;
  end

  def read_file_ex (name)
    gr = Graph.new
    file_name = self.select_file name
    file = File.open(__dir__ + file_name , 'r') 
    file.read.each_line do |line| 
      if line.include? ","
        edge = line.split ","
        v1 = Vertex.new edge[0]
        v2 = Vertex.new edge[1].chop

        vo = gr.add_vertex v1
        vf = gr.add_vertex v2
        
        vo.add_edge vf

      end
    end
    file.close
    gr
  end
end



